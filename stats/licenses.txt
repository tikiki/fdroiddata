GPLv3 759
Apache2 443
GPLv3+ 224
MIT 215
GPLv2 119
GPLv2+ 81
NewBSD 32
GPL 25
AGPLv3 16
MPL2 15
WTFPL 14
LGPL 13
PublicDomain 12
FreeBSD 11
AGPLv3+ 10
ISC 8
AGPL 6
BSD 5
LGPLv3 4
Artistic2 3
Unlicense 3
Beer License 2
GPL,Artistic 2
LGPLv2.1 2
MirOS 2
Zlib 2
Apache 1
Apache2,CC-BY-SA 1
Apache2,GPLv3+ 1
BSD,Apache2 1
BSD-3 1
Beerware 1
CC-BY-4.0 1
CC-BY-SA 1
CC-BY-SA-4.0 1
CC0 1
EPL 1
EUPL 1
Expat 1
Fair License 1
GNUFDL 1
GNUFDL,CC-BY-SA 1
GPL,MPL 1
GPLv3 or New BSD 1
GPLv3,Apache2,LGPL 1
GPLv3,CC-BY-SA-3.0 1
GPLv3,CC-BY-SA-4.0 1
LGPLv2.1+ 1
LGPLv2.1+,CC-BY-SA-3.0 1
LGPLv3,CC-BY-SA-4.0 1
MPL1.1 1
MissingLicense 1
NCSA 1
NYSLv0.9982 1
NetBSD 1
NetBSD,Apache2 1
Unlicense,Apache2 1
X11 1
