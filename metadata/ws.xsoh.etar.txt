Categories:Time
License:Apache2
Web Site:
Source Code:https://github.com/xsoh/Etar-Calendar
Issue Tracker:https://github.com/xsoh/Etar-Calendar/issues

Auto Name:Etar
Summary:Material designed calendar
Description:
Etar is material designed calendar based on the ASOP calendar.
.

Repo Type:git
Repo:https://github.com/xsoh/Etar-Calendar.git

Build:1.0.1,5
    commit=v1.0.1
    submodules=yes
    gradle=yes

Build:1.0.2,6
    commit=v1.0.2
    submodules=yes
    gradle=yes

Build:1.0.3,7
    commit=v1.0.3
    submodules=yes
    gradle=yes

Build:1.0.4,8
    commit=v1.0.4
    submodules=yes
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0.4
Current Version Code:8
