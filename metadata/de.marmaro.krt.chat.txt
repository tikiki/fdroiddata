Categories:Internet
License:GPLv3
Web Site:
Source Code:https://github.com/siacs/Conversations
Issue Tracker:https://github.com/siacs/Conversations/issues
Changelog:https://github.com/siacs/Conversations/blob/HEAD/CHANGELOG.md
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=CW3SYT3KG5PDL
FlattrID:3531619
Bitcoin:1NxSU1YxYzJVDpX1rcESAA3NJki7kRgeeu

Name:DEBUG
Auto Name:Conversations
Summary:XMPP client
Description:
'''Do not install -- this is a debugging build'''

'''Warning:''': This is an untested build that replaces colors at build time.
Using the dark theme should result in an AMOLED friendly black theme, but it has
unintended consequences.
.

Repo Type:git
Repo:https://github.com/siacs/Conversations.git

Build:1.13.5,162
    commit=1.13.5
    gradle=free
    prebuild=sed -i -e '/playstoreCompile/d' -e '/maven {/,+2d' build.gradle && \
        sed -i -e '/applicationId/s/eu.siacs.conversations/de.marmaro.krt.chat/g' build.gradle && \
        sed -i -e 's/259b24/000000/g' -e 's/0a7e07/000000/g' -e 's/026100/000000/g' -e 's/024500/000000/g' src/main/res/values/colors.xml && \
        sed -i -e 's/fafafa/000000/g' -e 's/eeeeee/000000/g' -e 's/9e9e9e/000000/g' -e 's/424242/000000/g' -e 's/282828/000000/g'  src/main/res/values/colors.xml && \
        sed -i -e 's/de000000/ff000000/g' -e 's/8a000000/ff000000/g' -e 's/42000000/ff000000/g' -e 's/1f000000/ff000000/g'  src/main/res/values/colors.xml

Build:1.13.6,163
    commit=1.13.6
    gradle=free
    prebuild=sed -i -e '/playstoreCompile/d' -e '/maven {/,+2d' build.gradle && \
        sed -i -e '/applicationId/s/eu.siacs.conversations/de.marmaro.krt.chat/g' build.gradle && \
        sed -i -e 's/259b24/000000/g' -e 's/0a7e07/000000/g' -e 's/026100/000000/g' -e 's/024500/000000/g' src/main/res/values/colors.xml && \
        sed -i -e 's/fafafa/000000/g' -e 's/eeeeee/000000/g' -e 's/9e9e9e/000000/g' -e 's/424242/000000/g' -e 's/282828/000000/g'  src/main/res/values/colors.xml && \
        sed -i -e 's/de000000/ff000000/g' -e 's/8a000000/ff000000/g' -e 's/42000000/ff000000/g' -e 's/1f000000/ff000000/g'  src/main/res/values/colors.xml

Build:1.13.6-git,164
    commit=3d372cb3394ce934cce2881f2bf428b302925431
    gradle=free
    prebuild=sed -i -e '/playstoreCompile/d' -e '/maven {/,+2d' build.gradle && \
        sed -i -e '/applicationId/s/eu.siacs.conversations/de.marmaro.krt.chat/g' -e 's/versionCode 163/versionCode 164/g' -e 's/versionName "1.13.6"/versionName "1.13.6-git"/g' build.gradle && \
        sed -i -e 's/259b24/000000/g' -e 's/0a7e07/000000/g' -e 's/026100/000000/g' -e 's/024500/000000/g' src/main/res/values/colors.xml && \
        sed -i -e 's/fafafa/000000/g' -e 's/eeeeee/000000/g' -e 's/9e9e9e/000000/g' -e 's/424242/000000/g' -e 's/282828/000000/g'  src/main/res/values/colors.xml && \
        sed -i -e 's/de000000/ff000000/g' -e 's/8a000000/ff000000/g' -e 's/42000000/ff000000/g' -e 's/1f000000/ff000000/g'  src/main/res/values/colors.xml

Build:1.13.7,165
    commit=1.13.7
    gradle=free
    prebuild=sed -i -e '/playstoreCompile/d' -e '/maven {/,+2d' build.gradle && \
        sed -i -e '/applicationId/s/eu.siacs.conversations/de.marmaro.krt.chat/g' -e 's/versionCode 163/versionCode 164/g' -e 's/versionName "1.13.6"/versionName "1.13.6-git"/g' build.gradle && \
        sed -i -e 's/259b24/000000/g' -e 's/0a7e07/000000/g' -e 's/026100/000000/g' -e 's/024500/000000/g' src/main/res/values/colors.xml && \
        sed -i -e 's/fafafa/000000/g' -e 's/eeeeee/000000/g' -e 's/9e9e9e/000000/g' -e 's/424242/000000/g' -e 's/282828/000000/g'  src/main/res/values/colors.xml && \
        sed -i -e 's/de000000/ff000000/g' -e 's/8a000000/ff000000/g' -e 's/42000000/ff000000/g' -e 's/1f000000/ff000000/g'  src/main/res/values/colors.xml

Archive Policy:0 versions
Auto Update Mode:Version %v
Update Check Mode:Tags
Update Check Name:eu.siacs.conversations
Current Version:1.13.7
Current Version Code:165
